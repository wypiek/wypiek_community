# Wypiek

## Wstęp

Witaj w oficjalnym repozytorium aplikacji Wypiek. Choć nasza aplikacja nie jest dostępna jako projekt open source, stworzyliśmy to repozytorium, aby ułatwić komunikację z użytkownikami. Dzięki temu możesz śledzić postępy w projekcie oraz zgłaszać wszelkie błędy lub problemy, które napotkałeś podczas korzystania z aplikacji.

## Jak zgłaszać błędy?

Jeśli napotkałeś błąd w naszej aplikacji lub masz sugestie dotyczące ulepszeń, bardzo prosimy o stworzenie nowego zgłoszenia w zakładce Plan > Issues. Do tego będzie wymagane konto na gitlab.

1. Upewnij się, że nikt wcześniej nie zgłosił podobnego problemu.
2. Stwórz nowe zgłoszenie, podając jak najwięcej informacji - opis problemu, kroki do jego odtworzenia oraz, jeśli to możliwe, zrzuty ekranu.
3. Prosimy o zgłaszanie błędów dotyczących aplikacji, nie samego portalu. Np. niepoprawne wyniki w wyszukiwarce dotyczyłyby funkcjonowania portalu, nie aplikacji.

## Aktualne prace

Chcesz wiedzieć nad czym obecnie pracujemy? W zakładce "Issues" możesz zobaczyć, jakie funkcje są w trakcie tworzenia bądź rozważań. Jeśli masz pomysły na nowe funkcje to zachęcamy do zgłaszania!

## Kontakt

Jeśli masz pytania dotyczące naszej aplikacji, które nie są związane z błędami lub problemami technicznymi, skontaktuj się z nami bezpośrednio poprzez wypiek@bakehaus.io lub wiadomością prywatną na [@bakehaus](https://wykop.pl/ludzie/bakehaus).

Dziękujemy za wsparcie i pomoc w udoskonalaniu Wypieku!